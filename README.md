# jdAptDirReinstall

This script helps you, if you accidentally removed a system directory. It will reinstall all packages, that had files in the directory.  
`Usage: jdAptDirReinstall <directory>`

This script needs apt and apt-file. Please run apt-file update before executing this script.